We are proud to be a local family owned and operated business. With over 70 years of experience, British Empire Fuels is the trusted name in air conditioners, furnaces, HVAC, fuel and propane delivery in Kawartha Lakes.

Address: 41 County Road 36, Bobcaygeon, ON K0M 1A0, Canada

Phone: 705-738-2121

Website: http://www.britishempirefuels.com
